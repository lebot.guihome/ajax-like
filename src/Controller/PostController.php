<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\PostLike;
use App\Repository\PostLikeRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findAll(),
        ]);
    }

    /**
     * like or dislike post
     *@Route("/post/{id}/like", name="post_like")
     * @param Post $post
     * @param EntityManagerInterface $em
     * @param PostLike $postLike
     */
    public function like(Post $post, EntityManagerInterface $em, PostLikeRepository $postLikeRepo)
    {
        $user = $this->getUser();
        if(!$user)return $this->json([
            'code'=>403,
            'message'=>"unauthorized"
        ],403);

        if($post->isLikedByUser($user)){
            $like = $postLikeRepo->findOneBy([
                'post'=>$post,
                'user'=>$user
            ]);
            $em->remove($like);
            $em->flush();
            return $this->json([
                'code'=> 200,
                'message'=> 'like suppimé',
                'likes'=> $postLikeRepo->count(['post'=>$post])

            ],200);
        }
        $like = new PostLike();
        $like->setPost($post)
            ->setUser($user);
        $em->persist($like);
        $em->flush();
        return $this->json([
            'code'=>200,
            'message'=>'like ajouté',
            'likes'=> $postLikeRepo->count(['post'=>$post])
            ],200);
    }
}
