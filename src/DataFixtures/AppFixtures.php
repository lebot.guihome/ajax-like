<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\PostLike;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
          
        private $encoder;
        public function __construct(UserPasswordEncoderInterface $encoder)
        {
            $this->encoder = $encoder;
        }
    public function load(ObjectManager $manager)
    {
            $users = [];
            $user = new User();
            $user->setEmail('bob@free.fr')
                ->setPassword($this->encoder->encodePassword($user, 'password'));
            $manager->persist($user);
            $users[] = $user;
        for($j=0; $j<10; $j++){
            $user = new User();
            $user->setEmail('bobby'.$j.'@free.fr')
            ->setPassword($this->encoder->encodePassword($user, 'password'));
            $manager->persist($user);
            $users[] = $user;
        }
        for($i=0; $i<10; $i++){
            $post = new Post();
            $post->setTitle('titre'.$i)
                 ->setIntroduction("intro".$i)
                 ->setContent('contenu'.$i);
            $manager->persist($post);
            for($k=0; $k<mt_rand(2,5); $k++){
                $like = new PostLike();
                $like->setPost($post)
                    ->setUser($user);
                $manager->persist($like);
            }
        }
        $manager->flush();
    }

}